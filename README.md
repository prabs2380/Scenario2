# Application Deployment on Local Kubernetes

This application is a simple Nginx server that displays a greeting message using an environment variable and project to show increment counter

## Assumptions

1. A Kubernetes cluster is already set up and running locally using Minikube
2. Docker is installed and configured correctly.
3. Enable the Ingress controller with the command minikube addons enable ingress.


## Building the Docker Image

To build the Docker image for the application, navigate to the directory containing the Dockerfile 

```bash
docker build -t docker.io/pkesavan/counter:1.0 .
```

Incrementing the Version
To increment the version of the application from 1.0 to 2.0, you need to make your changes to the application, then rebuild the Docker image with the new version number:
```bash
docker build -t docker.io/pkesavan/counter:2.0 .
docker push pkesavan/counter:2.0
```

## Deployment

To deploy the application, modify the image tag of the nginx deployment file

```bash 
# Apply the yaml files
kubectl apply -f k8s/

# Verify the deployment
kubectl get deploy,svc,ingress

# Troubleshoot the logs
kubectl logs svc/nginx-service 
kubectl logs svc/mysql-service 

# verify the table name
kubectl exec -it svc/mysql-service -- mysql -u root -p"*******"
mysql: [Warning] Using a password on the command line interface can be insecure.
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 10
Server version: 8.0.32 MySQL Community Server - GPL

Copyright (c) 2000, 2023, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> USE testdb;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
mysql> SHOW TABLES;
+------------------+
| Tables_in_testdb |
+------------------+
| test_table       |
+------------------+
1 row in set (0.00 sec)

```

# Grafana

For more information about Grafana configuration, see the [Grafana README](grafana/README.md).

# Prometheus

For more information about Grafana configuration, see the [Prometheus README](Prometheus/README.md).