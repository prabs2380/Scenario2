# Grafana Helm Chart

* Installs the web dashboarding system [Grafana](http://grafana.org/)

## Get Repo Info

```console
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update
```

_See [helm repo](https://helm.sh/docs/helm/helm_repo/) for command documentation._

## Installing the Chart

```console
helm install dashboard grafana/grafana --namespace monitoring --create-namespace
```

## Upgrading the Chart

```console
helm upgrade dashboard grafana/grafana --namespace monitoring
```

## Uninstalling the Chart

```console
helm delete dashboard --namespace monitoring 
```
