# Prometheus

```console
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
```
## Install Chart

```console
helm install monitor prometheus-community/prometheus --namespace monitoring --create-namespace
```

## Uninstall Chart

```console
helm uninstall monitor --namespace monitoring
```

## Upgrading Chart

```console
helm upgrade monitor prometheus-community/prometheus --namespace monitoring --install
```
