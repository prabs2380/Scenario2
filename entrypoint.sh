#!/bin/sh

# Replace placeholder with environment variable value
envsubst < /usr/share/nginx/html/index.html.template > /usr/share/nginx/html/index.html

# Start Nginx and keep it from running in the background
nginx -g 'daemon off;'