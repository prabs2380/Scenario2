FROM nginx:alpine

ENV FIRST_NAME="First Name"

COPY index.html /usr/share/nginx/html/index.html.template

COPY entrypoint.sh /entrypoint.sh

RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]